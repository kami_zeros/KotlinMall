package com.zeros.mykotlinmall.ui.adapter

import android.content.Context
import android.graphics.Paint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.zeros.baselibrary.ui.adapter.BaseRecyclerViewAdapter
import com.zeros.baselibrary.utils.GlideUtils
import com.zeros.mykotlinmall.R
import kotlinx.android.synthetic.main.layout_home_discount_item.view.*

/**
 *  首页折扣区域Adapter  ▏2018/5/24.
 *  wc
 */
class HomeDiscountAdapter(context: Context) : BaseRecyclerViewAdapter<String, HomeDiscountAdapter.ViewHolder>(context) {

    /*-创造视图-*/
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(mContext)
                .inflate(R.layout.layout_home_discount_item, parent, false)
        return ViewHolder(view)
    }

    /**
     * 数据已经在Baseadapter中设置了setData,所以直接调用dataList
     */
    /*-绑定数据-*/
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        //加载图片---通过holder拿到当前视图view
        GlideUtils.loadUrlImage(mContext, dataList[position], holder.itemView.mGoodsIconIv)

        //静态假数据
        holder.itemView.mDiscountAfterTv.text = "￥123.00"
        holder.itemView.mDiscountBeforeTv.text = "$1000.00"
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        init {
            //设置'原价'TextView样式
            view.mDiscountBeforeTv.paint.flags = Paint.STRIKE_THRU_TEXT_FLAG  //中划线
            view.mDiscountBeforeTv.paint.isAntiAlias = true
        }
    }

}