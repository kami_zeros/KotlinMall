package com.zeros.mykotlinmall.ui.fragment

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.youth.banner.BannerConfig
import com.youth.banner.Transformer
import com.zeros.baselibrary.ui.fragment.BaseFragment
import com.zeros.baselibrary.widgets.BannerImageLoader
import com.zeros.mykotlinmall.R
import com.zeros.mykotlinmall.common.*
import com.zeros.mykotlinmall.ui.adapter.HomeDiscountAdapter
import com.zeros.mykotlinmall.ui.adapter.TopicAdapter
import kotlinx.android.synthetic.main.fragment_home.*
import me.crosswall.lib.coverflow.CoverFlow

/**
 *  主界面Fragment  ▏2018/5/24.
 */
class HomeFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_home, null)
    }

    /*-视图渲染完成后，才能设置其他view-*/
    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initBanner()
        initNews()
        initDiscount()
        initTopic()
    }

    /*-1.初始化Banner-*/
    private fun initBanner() {
        //设置banner样式--指示器
//        mHomeBanner.setBannerStyle(BannerConfig.CIRCLE_INDICATOR_TITLE);//显示圆形指示器和标题（垂直显示）
//        mHomeBanner.setBannerStyle(BannerConfig.NUM_INDICATOR);//显示数字指示器
        //设置图片加载器
        mHomeBanner.setImageLoader(BannerImageLoader())
        //设置图片集合
        mHomeBanner.setImages(listOf(HOME_BANNER_ONE, HOME_BANNER_TWO, HOME_BANNER_THREE, HOME_BANNER_FOUR));

        //设置banner动画效果
        mHomeBanner.setBannerAnimation(Transformer.Accordion);
        //设置标题集合（当banner样式有显示title时）
//        mHomeBanner.setBannerTitles(titles);
        //设置自动轮播，默认为true
//        mHomeBanner.isAutoPlay(true);
        //设置轮播时间
        mHomeBanner.setDelayTime(1500);
        //设置指示器位置（当banner模式中有指示器时）
        mHomeBanner.setIndicatorGravity(BannerConfig.RIGHT);
        //banner设置方法全部调用完毕时最后调用
        mHomeBanner.start()
    }

    /*-2.初始化公告-*/
    private fun initNews() {
        //公告
        mNewsFlipperView.setData(arrayOf("夏日炎炎，第一波福利还有30秒到达战场", "新用户立领1000元优惠券"))
    }

    /*-3.初始化折扣(RecyclerView)-*/
    private fun initDiscount() {
        val manager = LinearLayoutManager(context)
        manager.orientation = LinearLayoutManager.HORIZONTAL
        mHomeDiscountRv.layoutManager = manager     //RecyclerView设置manage

        val discountAdapter = HomeDiscountAdapter(activity)
        mHomeDiscountRv.adapter = discountAdapter   //RecyclerView设置adapter

        //设置数据
        discountAdapter.setData(
                mutableListOf(HOME_DISCOUNT_ONE, HOME_DISCOUNT_TWO,
                        HOME_DISCOUNT_THREE, HOME_DISCOUNT_FOUR,
                        HOME_DISCOUNT_FIVE)
        )

    }

    /*-4,初始化主题（主内容）-*/
    private fun initTopic() {
        //话题
        mTopicPager.adapter = TopicAdapter(context, listOf(HOME_TOPIC_ONE, HOME_TOPIC_TWO,
                HOME_TOPIC_THREE, HOME_TOPIC_FOUR, HOME_TOPIC_FIVE))
        mTopicPager.currentItem = 1
        mTopicPager.offscreenPageLimit = 5  //viewpage上限5个

        //画廊
        CoverFlow.Builder().with(mTopicPager)
                .scale(0.3f)
                .pagerMargin(-30.0f)
                .spaceSize(0.0f)
                .build()
    }

}