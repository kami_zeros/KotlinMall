package com.zeros.mykotlinmall.ui.adapter

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.zeros.baselibrary.ext.loadUrl
import com.zeros.mykotlinmall.R
import kotlinx.android.synthetic.main.layout_topic_item.view.*

/**
 *  画廊-的viewpage的adapter  ▏2018/5/24.
 *  wc
 */
class TopicAdapter(private val context: Context, private val list: List<String>) : PagerAdapter() {

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val rootView = LayoutInflater.from(context).inflate(R.layout.layout_topic_item, null)
        //扩展方法--imageview的
        rootView.mTopicIv.loadUrl(list[position])
        container.addView(rootView)
        return rootView
    }

    override fun isViewFromObject(view: View?, paramObject: Any?): Boolean {
        return view == paramObject
    }

    override fun getCount(): Int {
        return this.list.size
    }

    override fun destroyItem(container: ViewGroup, position: Int, paramObject: Any?) {
        container.removeView(paramObject as View)
    }

}