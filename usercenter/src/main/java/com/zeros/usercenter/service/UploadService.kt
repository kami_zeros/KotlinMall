package com.zeros.usercenter.service

import rx.Observable

/**
 *  上传业务接口  ▏2018/5/23.
 *  wc
 */
interface UploadService {
    fun getUploadToken(): Observable<String>
}