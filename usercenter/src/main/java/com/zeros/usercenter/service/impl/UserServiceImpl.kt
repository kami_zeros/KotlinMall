package com.zeros.usercenter.service.impl

import com.zeros.baselibrary.data.protocol.BaseResp
import com.zeros.baselibrary.ext.convert
import com.zeros.baselibrary.ext.convertBoolean
import com.zeros.baselibrary.rx.BaseException
import com.zeros.baselibrary.rx.BaseFuncBoolean
import com.zeros.usercenter.data.protocol.UserInfo
import com.zeros.usercenter.data.respository.UserRepository
import com.zeros.usercenter.service.UserService
import rx.Observable
import rx.functions.Func1

/**
 *  用户模块业务-实现类  ▏2018/5/16.
 *  wc
 */
class UserServiceImpl : UserService {

    // lateinit可以在任何位置初始化并且可以初始化多次。而lazy在第一次被调用时就被初始化，想要被改变只能重新定义
    private val repository by lazy {
        UserRepository()
    }

    /*-1.注册-*/
    override fun register(mobile: String, pwd: String, verifyCode: String): Observable<Boolean> {
        return repository.register(mobile, pwd, verifyCode)
                //rx的数据转换
//                .flatMap(BaseFuncBoolean()) //1.原始
                .convertBoolean()   //2.这还是利用扩展函数
    }

    /*-2.登录-*/
    override fun login(mobile: String, pwd: String, pushId: String): Observable<UserInfo> {
        return repository.login(mobile, pwd, pushId)
                .convert()
    }

    /*-3.忘记密码-*/
    override fun forgetPwd(mobile: String, verifyCode: String): Observable<Boolean> {
        return repository.forgetPwd(mobile, verifyCode)
                .convertBoolean()
    }

    /*-4.重置密码-*/
    override fun resetPwd(mobile: String, pwd: String): Observable<Boolean> {
        return repository.resetPwd(mobile, pwd)
                .convertBoolean()
    }

    /*-5.修改用户资料-*/
    override fun editUser(userIcon: String, userName: String, userGender: String, userSign: String): Observable<UserInfo> {
        return repository.editUser(userIcon, userName, userGender, userSign)
                .convert()
    }


}