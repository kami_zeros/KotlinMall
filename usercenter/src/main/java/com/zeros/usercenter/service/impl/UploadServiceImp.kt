package com.zeros.usercenter.service.impl

import com.zeros.baselibrary.ext.convert
import com.zeros.usercenter.data.respository.UploadRepository
import com.zeros.usercenter.data.respository.UserRepository
import com.zeros.usercenter.service.UploadService
import rx.Observable

/**
 * 上传业务实现类   ▏2018/5/23.
 * wc
 */
class UploadServiceImp : UploadService {

    // lateinit可以在任何位置初始化并且可以初始化多次。
    // 而lazy在第一次被调用时就被初始化，想要被改变只能重新定义
    private val repository by lazy {
        UploadRepository()
    }


    override fun getUploadToken(): Observable<String> {
        return repository.getUploadToken()
                .convert()
    }

}