package com.zeros.usercenter.ui.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.zeros.baselibrary.common.AppManager
import com.zeros.baselibrary.ext.enable
import com.zeros.baselibrary.ext.onClick
import com.zeros.baselibrary.ui.activity.BaseMvpActivity
import com.zeros.usercenter.R
import com.zeros.usercenter.presenter.RegisterPresenter
import com.zeros.usercenter.presenter.view.RegisterView
import kotlinx.android.synthetic.main.activity_register.*
import org.jetbrains.anko.toast

/**
 * 注册
 * wc
 */
class RegisterActivity : BaseMvpActivity<RegisterPresenter>(), RegisterView, View.OnClickListener {

    private var pressTime: Long = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        mPresenter = RegisterPresenter()//初始化Presenter
        mPresenter.mView = this   //初始化view,让此activity实现view

        initView()
//        1.原始点击方法
//        mRegisterBtn.setOnClickListener {
//            mPresenter.register("aaa", "ccc", "")
//        }

//        2.通过扩展函数的点击方法
//        mRegisterBtn.onClick(object : View.OnClickListener {
//            override fun onClick(v: View?) {
//                mPresenter.register("aaa", "ccc", "")
//            }
//        })

//        3.通过扩展函数的点击方法
//        mRegisterBtn.onClick {
//            mPresenter.register("aaa", "ccc", "")
//        }
    }

    /*-初始化视图-*/
    private fun initView() {
        //设置注册按钮是否-可点击
        mRegisterBtn.enable(mMobileEt, { isBtnEnable() })
        mRegisterBtn.enable(mVerifyCodeEt, { isBtnEnable() })
        mRegisterBtn.enable(mPwdEt, { isBtnEnable() })
        mRegisterBtn.enable(mPwdConfirmEt, { isBtnEnable() })

        mRegisterBtn.onClick(this)       //注册按钮
        mVerifyCodeBtn.onClick(this)     //发送验证码
    }

    /*-点击事件-*/
    override fun onClick(v: View) {
        when (v.id) {
        //注册按钮
            R.id.mRegisterBtn -> {
                mPresenter.register(mMobileEt.text.toString(), mPwdEt.text.toString(), mPwdConfirmEt.text.toString())
            }
        //发送验证码
            R.id.mVerifyCodeBtn -> {
                mVerifyCodeBtn.requestSendVerifyNumber()
                //todo 需要与服务器交互---此处省略了
                toast("发送验证成功")
            }
        }
    }


    /*-注册回调-*/
    override fun onRegisterResult(result: String) {
        toast(result)
        finish()
    }

    /*-编辑框是否为空：按钮是否可用*/
    fun isBtnEnable(): Boolean {
        return mMobileEt.text.isNullOrEmpty().not() &&
                mVerifyCodeEt.text.isNullOrEmpty().not() &&
                mPwdEt.text.isNullOrEmpty().not() &&
                mPwdConfirmEt.text.isNullOrEmpty().not()
    }


    /*-双击退出按钮-*/
    override fun onBackPressed() {
        val time = System.currentTimeMillis()

        if (time - pressTime > 2000) {
            toast("再按一次退出程序")
            pressTime = time
        } else {
            AppManager.instance.exitApp(this)
        }
    }

}
