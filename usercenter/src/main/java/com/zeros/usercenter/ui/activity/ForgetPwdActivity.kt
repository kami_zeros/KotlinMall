package com.zeros.usercenter.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.zeros.baselibrary.ext.enable
import com.zeros.baselibrary.ext.onClick
import com.zeros.baselibrary.ui.activity.BaseMvpActivity
import com.zeros.usercenter.R
import com.zeros.usercenter.presenter.ForgetPwdPresenter
import com.zeros.usercenter.presenter.RegisterPresenter
import com.zeros.usercenter.presenter.view.ForgetPwdView
import kotlinx.android.synthetic.main.activity_forget_pwd.*
import org.jetbrains.anko.toast

/**
 * 忘记密码界面
 * wc
 */
class ForgetPwdActivity : BaseMvpActivity<ForgetPwdPresenter>(), ForgetPwdView, View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forget_pwd)

        mPresenter = ForgetPwdPresenter()//初始化Presenter
        mPresenter.mView = this   //初始化view,让此activity实现view

        initView()
    }

    /*-初始化视图-*/
    private fun initView() {
        mNextBtn.enable(mMobileEt, { isBtnEnable() })
        mNextBtn.enable(mVerifyCodeEt, { isBtnEnable() })

        mVerifyCodeBtn.onClick(this)
        mNextBtn.onClick(this)
    }

    /*-点击事件-*/
    override fun onClick(v: View) {
        when (v.id) {
        //发送验证码
            R.id.mVerifyCodeBtn -> {
                mVerifyCodeBtn.requestSendVerifyNumber()
            }
        //发送成功后--跳转界面
            R.id.mNextBtn -> {
                mPresenter.forgetPwd(mMobileEt.text.toString(), mVerifyCodeEt.text.toString())
            }
        }
    }


    /*-判断编辑框是否为空-按钮是否可用-*/
    private fun isBtnEnable(): Boolean {
        return mMobileEt.text.isNullOrEmpty().not() &&
                mVerifyCodeEt.text.isNullOrEmpty().not()
    }


    /*-忘记密码回调-跳转(重置密码)界面-*/
    override fun onForgetPwdResult(result: String) {
        toast(result)
        val intent = Intent(this@ForgetPwdActivity, ResetPwdActivity::class.java)
        intent.putExtra("mobile", mMobileEt.text.toString())
        startActivity(intent)
    }

}
