package com.zeros.usercenter.ui.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.zeros.baselibrary.ext.enable
import com.zeros.baselibrary.ext.onClick
import com.zeros.baselibrary.ui.activity.BaseMvpActivity
import com.zeros.usercenter.R
import com.zeros.usercenter.presenter.ResetPwdPresenter
import com.zeros.usercenter.presenter.view.ResetPwdView
import kotlinx.android.synthetic.main.activity_reset_pwd.*
import org.jetbrains.anko.toast


/**
 * 重置密码界面
 * wc
 */
class ResetPwdActivity : BaseMvpActivity<ResetPwdPresenter>(), ResetPwdView {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_pwd)

        mPresenter = ResetPwdPresenter()
        mPresenter.mView = this

        initView()
    }

    /*初始化视图*/
    private fun initView() {
        var mobile = intent.getStringExtra("mobile")

        mConfirmBtn.enable(mPwdEt, { isBtnEnable() })
        mConfirmBtn.enable(mPwdConfirmEt,{ isBtnEnable()})

        mConfirmBtn.onClick {
            if (mPwdEt.text.toString() != mPwdConfirmEt.text.toString()) {
                toast("密码不一致")
                return@onClick
            }
            mPresenter.resetPwd(mobile, mPwdEt.text.toString())
        }
    }

    /*-判断编辑框是否为空-按钮是否可用*/
    private fun isBtnEnable(): Boolean {
        return mPwdEt.text.isNullOrEmpty().not() &&
                mPwdConfirmEt.text.isNullOrEmpty().not()
    }

    /*重置密码回调--跳转到登录界面-*/
    override fun onResetPwdResult(result: String) {
        toast(result)
        val intent = Intent(this@ResetPwdActivity, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

        startActivity(intent)
        finish()
    }

}
