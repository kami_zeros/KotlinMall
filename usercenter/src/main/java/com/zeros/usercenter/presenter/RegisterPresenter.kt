package com.zeros.usercenter.presenter

import android.util.Log
import com.zeros.baselibrary.ext.excute
import com.zeros.baselibrary.presenter.BasePresenter
import com.zeros.baselibrary.rx.BaseSubscriber
import com.zeros.usercenter.presenter.view.RegisterView
import com.zeros.usercenter.service.UserService
import com.zeros.usercenter.service.impl.UserServiceImpl

/**
 *  用户注册Presenter  ▏2018/5/15.
 *  wc
 */
class RegisterPresenter : BasePresenter<RegisterView>() {

    fun register(mobile: String, pwd: String, verifyCode: String) {
        /*-业务逻辑-*/
        if (!checkNetWork()) {
            return
        }

        val userService = UserServiceImpl()
        mView.showLoading()

        //1.扩展函数方法
        userService.register(mobile, pwd, verifyCode)
                //此处是利用了扩展函数
                .excute(object : BaseSubscriber<Boolean>(mView) {
                    override fun onNext(t: Boolean) {
                        if (t)
                            mView.onRegisterResult("注册成功")
                    }
                }, lifecycleProvider)

        //2.最原始的的方法：只是利用了封装的形式
//                .observeOn(Schedulers.io())
//                .subscribeOn(AndroidSchedulers.mainThread())
//                .subscribe(object : BaseSubscriber<Boolean>() {
//                    override fun onNext(t: Boolean) {
//                        if (t)
//                            mView.onRegisterResult("注册成功")
//                    }
//                })
    }

}