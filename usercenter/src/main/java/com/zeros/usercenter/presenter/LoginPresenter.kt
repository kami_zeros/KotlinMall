package com.zeros.usercenter.presenter

import com.zeros.baselibrary.ext.excute
import com.zeros.baselibrary.presenter.BasePresenter
import com.zeros.baselibrary.rx.BaseSubscriber
import com.zeros.usercenter.data.protocol.UserInfo
import com.zeros.usercenter.presenter.view.LoginView
import com.zeros.usercenter.service.impl.UserServiceImpl

/**
 *  登录界面 Presenter  ▏2018/5/22.
 *  wc
 */
class LoginPresenter : BasePresenter<LoginView>() {

    fun login(mobile: String, pwd: String, pushId: String) {
        if (!checkNetWork()) {
            return
        }
        mView.showLoading()
        val userService = UserServiceImpl()

        userService.login(mobile, pwd, pushId)
                .excute(object : BaseSubscriber<UserInfo>(mView) {
                    override fun onNext(t: UserInfo) {
                        mView.onLoginResult(t)
                    }

                    override fun onError(e: Throwable?) {
                        super.onError(e)
                        mView.onLoginResult(UserInfo("1", "", "",
                                "", "", ""))

                    }
                }, lifecycleProvider)
    }

}