package com.zeros.usercenter.presenter.view

import com.zeros.baselibrary.presenter.view.BaseView

/**
 *  忘记密码 视图回调  ▏2018/5/22.
 *  wc
 */
interface ForgetPwdView : BaseView {

    //忘记密码回调
    fun onForgetPwdResult(result: String)

}