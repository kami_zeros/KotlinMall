package com.zeros.usercenter.presenter.view

import com.zeros.baselibrary.presenter.view.BaseView
import com.zeros.usercenter.data.protocol.UserInfo

/**
 *  用户登录 视图回调  ▏2018/5/22.
 *  wc
 */
interface LoginView : BaseView {
    fun onLoginResult(result: UserInfo)
}