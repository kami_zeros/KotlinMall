package com.zeros.usercenter.presenter

import com.zeros.baselibrary.ext.excute
import com.zeros.baselibrary.presenter.BasePresenter
import com.zeros.baselibrary.rx.BaseSubscriber
import com.zeros.usercenter.presenter.view.ForgetPwdView
import com.zeros.usercenter.service.impl.UserServiceImpl

/**
 *  忘记密码Presenter  ▏2018/5/22.
 *  wc
 */
class ForgetPwdPresenter : BasePresenter<ForgetPwdView>() {

    fun forgetPwd(mobile: String, verifyCode: String) {
        if (!checkNetWork()) {
            return
        }
        val userService = UserServiceImpl()
        mView.showLoading()

        userService.forgetPwd(mobile, verifyCode)
                .excute(object : BaseSubscriber<Boolean>(mView) {
                    override fun onNext(t: Boolean) {
                        if (t)
                            mView.onForgetPwdResult("验证成功")
                    }

                    //测试--因为是重写了所以还是必须hideLoading
                    override fun onError(e: Throwable?) {
                        mView.hideLoading()
                        mView.onForgetPwdResult("验证成功error")
                    }
                }, lifecycleProvider)
    }

}