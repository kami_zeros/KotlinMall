package com.zeros.usercenter.presenter.view

import com.zeros.baselibrary.presenter.view.BaseView

/**
 *  用户注册 视图回调  ▏2018/5/15.
 *  wc
 */
interface RegisterView : BaseView {

    fun onRegisterResult(result: String)

}