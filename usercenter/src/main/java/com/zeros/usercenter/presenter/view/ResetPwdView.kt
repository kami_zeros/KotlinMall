package com.zeros.usercenter.presenter.view

import com.zeros.baselibrary.presenter.view.BaseView

/**
 *  重置密码 视图回调  ▏2018/5/22.
 *  wc
 */
interface ResetPwdView : BaseView {
    fun onResetPwdResult(result: String)
}