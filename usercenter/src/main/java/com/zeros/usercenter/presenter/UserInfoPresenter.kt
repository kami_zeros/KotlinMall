package com.zeros.usercenter.presenter

import com.qiniu.android.storage.UploadManager
import com.zeros.baselibrary.ext.excute
import com.zeros.baselibrary.presenter.BasePresenter
import com.zeros.baselibrary.rx.BaseSubscriber
import com.zeros.usercenter.data.protocol.UserInfo
import com.zeros.usercenter.presenter.view.UserInfoView
import com.zeros.usercenter.service.UploadService
import com.zeros.usercenter.service.UserService
import com.zeros.usercenter.service.impl.UploadServiceImp
import com.zeros.usercenter.service.impl.UserServiceImpl

/**
 *  编辑用户资料Presenter  ▏2018/5/23.
 *  wc
 */
class UserInfoPresenter : BasePresenter<UserInfoView>() {

    private val userService: UserService by lazy {
        UserServiceImpl()
    }

    private val uploadService: UploadService by lazy {
        UploadServiceImp()
    }


    /*-1.编辑用户资料-*/
    fun editUser(userIcon: String, userName: String, userGender: String, userSign: String) {
        if (!checkNetWork())
            return
        mView.showLoading()
//        userService=UserServiceImpl()//可以在这初始化

        userService.editUser(userIcon, userName, userGender, userSign)
                .excute(object : BaseSubscriber<UserInfo>(mView) {
                    override fun onNext(t: UserInfo) {
                        mView.onEditUserResult(t)
                    }
                }, lifecycleProvider)
    }

    /*-2.获取七牛云上传凭证-*/
    fun getUploadToken() {
        if (!checkNetWork())
            return
        mView.showLoading()
        uploadService.getUploadToken()
                .excute(object : BaseSubscriber<String>(mView) {
                    override fun onNext(t: String) {
                        mView.onGetUploadTokenResult(t)
                    }
                }, lifecycleProvider)
    }

}