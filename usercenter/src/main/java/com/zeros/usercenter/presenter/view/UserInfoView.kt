package com.zeros.usercenter.presenter.view

import com.zeros.baselibrary.presenter.view.BaseView
import com.zeros.usercenter.data.protocol.UserInfo

/**
 *  编辑用户资料 视图回调  ▏2018/5/23.
 *  wc
 */
interface UserInfoView : BaseView {

    /*  获取上传凭证回调 */
    fun onGetUploadTokenResult(result: String)

    /*  编辑用户资料回调  */
    fun onEditUserResult(result: UserInfo)

}