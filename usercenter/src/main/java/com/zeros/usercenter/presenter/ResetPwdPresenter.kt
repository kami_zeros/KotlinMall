package com.zeros.usercenter.presenter

import com.zeros.baselibrary.ext.excute
import com.zeros.baselibrary.presenter.BasePresenter
import com.zeros.baselibrary.rx.BaseSubscriber
import com.zeros.usercenter.presenter.view.ResetPwdView
import com.zeros.usercenter.service.impl.UserServiceImpl

/**
 *  重置密码Presenter  ▏2018/5/22.
 *  wc
 */
class ResetPwdPresenter : BasePresenter<ResetPwdView>() {

    /*重置密码*/
    fun resetPwd(mobile: String, pwd: String) {
        if (!checkNetWork()) {
            return
        }
        mView.showLoading()
        val userService = UserServiceImpl()

        userService.resetPwd(mobile, pwd)
                .excute(object : BaseSubscriber<Boolean>(mView) {
                    override fun onNext(t: Boolean) {
                        if (t)
                            mView.onResetPwdResult("重置密码成功")
                    }
                }, lifecycleProvider)
    }

}