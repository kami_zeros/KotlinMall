package com.zeros.usercenter.data.protocol

/**
 *  登录请求体  ▏2018/5/22.
 *  wc
 */
data class LoginReq(val mobile:String, val pwd:String, val pushId:String)