package com.zeros.usercenter.data.api

import com.zeros.baselibrary.data.protocol.BaseResp
import com.zeros.usercenter.data.protocol.*
import retrofit2.http.Body
import retrofit2.http.POST
import rx.Observable

/**
 * 用户相关 接口   ▏2018/5/16.
 * wc
 */
interface UserApi {

    /*-1.用户注册-*/
    @POST("userCenter/register")
    fun register(@Body req: RegisterReq): Observable<BaseResp<String>>

    /*-2.用户登录-*/
    @POST("userCenter/login")
    fun login(@Body req: LoginReq): Observable<BaseResp<UserInfo>>

    /*-3.忘记密码-*/
    @POST("userCenter/forgetPwd")
    fun forgetPwd(@Body req: ForgetPwdReq): Observable<BaseResp<String>>

    /*-4.重置密码-*/
    @POST("userCenter/resetPwd")
    fun resetPwd(@Body req: ResetPwdReq): Observable<BaseResp<String>>

    /*-5.编辑用户资料-*/
    @POST("userCenter/editUser")
    fun editUser(@Body req: EditUserReq): Observable<BaseResp<UserInfo>>


}