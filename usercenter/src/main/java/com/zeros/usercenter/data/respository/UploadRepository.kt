package com.zeros.usercenter.data.respository

import com.zeros.baselibrary.data.net.RetrofitFactory
import com.zeros.baselibrary.data.protocol.BaseResp
import com.zeros.usercenter.data.api.UploadApi
import rx.Observable

/**
 *  上传相关 数据层  ▏2018/5/23.
 *  wc
 */
class UploadRepository {

    //获取七牛云上传凭证
    fun getUploadToken(): Observable<BaseResp<String>> {
        return RetrofitFactory.instance
                .create(UploadApi::class.java)
                .getUploadToken()
    }

}