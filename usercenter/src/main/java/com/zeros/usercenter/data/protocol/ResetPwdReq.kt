package com.zeros.usercenter.data.protocol

/**
 *  重置密码请求体  ▏2018/5/22.
 *  wc
 */
data class ResetPwdReq(val mobile: String, val pwd: String)