package com.zeros.usercenter.data.api

import com.zeros.baselibrary.data.protocol.BaseResp
import retrofit2.http.POST
import rx.Observable

/**
 *  上传相关 接口  ▏2018/5/23.
 *  wc
 */
interface UploadApi {

    /*-获取七牛云上传凭证-*/
    @POST("common/getUploadToken")
    fun getUploadToken(): Observable<BaseResp<String>>

}