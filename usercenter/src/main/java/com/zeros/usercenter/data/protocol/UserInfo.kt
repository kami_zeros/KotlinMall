package com.zeros.usercenter.data.protocol

/**
 *  用户实体类  ▏2018/5/22.
 *  wc
 */

data class UserInfo(
        val id: String,
        val userIcon: String,
        val userName: String,
        val userGender: String,
        val userMobile: String,
        val userSign: String
)