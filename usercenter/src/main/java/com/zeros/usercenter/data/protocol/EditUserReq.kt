package com.zeros.usercenter.data.protocol

/**
 *  修改用户资料请求体  ▏2018/5/23.
 *  wc
 */
data class EditUserReq(val userIcon: String,
                       val userName: String,
                       val gender: String,
                       val sign: String)