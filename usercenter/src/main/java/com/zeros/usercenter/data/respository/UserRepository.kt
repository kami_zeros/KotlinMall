package com.zeros.usercenter.data.respository

import com.zeros.baselibrary.data.net.RetrofitFactory
import com.zeros.baselibrary.data.protocol.BaseResp
import com.zeros.usercenter.data.api.UserApi
import com.zeros.usercenter.data.protocol.*
import rx.Observable

/**
 * 真正访问数据网络的类
 *  用户相关数据层  ▏2018/5/16.
 *  wc
 */
class UserRepository {

    /*-1.用户注册-*/
    fun register(mobile: String, ped: String, verifyCode: String): Observable<BaseResp<String>> {
        return RetrofitFactory.instance
                .create(UserApi::class.java)
                .register(RegisterReq(mobile, ped, verifyCode))
    }

    /*-2.用户登录-*/
    fun login(mobile: String, pwd: String, pushId: String): Observable<BaseResp<UserInfo>> {
        return RetrofitFactory.instance
                .create(UserApi::class.java)
                .login(LoginReq(mobile, pwd, pushId))
    }

    /*-3.忘记密码-*/
    fun forgetPwd(mobile: String, verifyCode: String): Observable<BaseResp<String>> {
        return RetrofitFactory.instance
                .create(UserApi::class.java)
                .forgetPwd(ForgetPwdReq(mobile, verifyCode))
    }

    /*-4.重置密码-*/
    fun resetPwd(mobile: String, pwd: String): Observable<BaseResp<String>> {
        return RetrofitFactory.instance
                .create(UserApi::class.java)
                .resetPwd(ResetPwdReq(mobile, pwd))
    }

    /*-5.编辑用户资料-*/
    fun editUser(userIcon: String, userName: String, userGender: String, userSign: String): Observable<BaseResp<UserInfo>> {
        return RetrofitFactory.instance
                .create(UserApi::class.java)
                .editUser(EditUserReq(userIcon, userName, userGender, userSign))
    }

}