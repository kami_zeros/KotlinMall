package com.zeros.usercenter.data.protocol

/**
 *  忘记密码请求体  ▏2018/5/22.
 *  wc
 */
data class ForgetPwdReq(val mobile: String, val verifyCode: String)