package com.zeros.usercenter.data.protocol

/**
 *  注册请求体  ▏2018/5/16.
 *  data class--就像Java中的Bean对象
 *      编译器会自动生成方法:equals()/hashCode() /toString()等
 *  wc
 */
data class RegisterReq(val mobile: String, val ped: String, val verifyCode: String)
