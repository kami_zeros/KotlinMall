package com.zeros.usercenter.utils

import com.zeros.baselibrary.common.BaseConstant
import com.zeros.baselibrary.utils.AppPrefsUtils
import com.zeros.provider.common.ProviderConstant
import com.zeros.usercenter.data.protocol.UserInfo

/**
 *  本地存储用户相关信息  ▏2018/5/23.
 *  wc
 */
object UserPrefsUtils {

    /*- 退出登录时，传入null,清空存储-*/
    fun putUserInfo(userInfo: UserInfo?) {
        //userInfo不是空，返回userInfo.id。userInfo是空时返回""
        AppPrefsUtils.putString(BaseConstant.KEY_SP_TOKEN, userInfo?.id ?: "")

        AppPrefsUtils.putString(ProviderConstant.KEY_SP_USER_ICON, userInfo?.userIcon ?: "")
        AppPrefsUtils.putString(ProviderConstant.KEY_SP_USER_NAME, userInfo?.userName ?: "")
        AppPrefsUtils.putString(ProviderConstant.KEY_SP_USER_MOBILE, userInfo?.userMobile ?: "")
        AppPrefsUtils.putString(ProviderConstant.KEY_SP_USER_GENDER, userInfo?.userGender ?: "")
        AppPrefsUtils.putString(ProviderConstant.KEY_SP_USER_SIGN, userInfo?.userSign ?: "")
    }

}