package com.zeros.baselibrary.data.protocol

/**
 *  能用响应对象
 *  @status:响应状态码
 *  @message:响应文字消息
 *  @data:具体响应业务对象  ▏2018/5/16.
 *
 *  data class--就像Java中的Bean对象
 *      编译器会自动生成方法:equals()/hashCode() /toString()等
 *  out:??
 *  wc
 */
data class BaseResp<out T>(val status: Int, val message: String, val data: T)