package com.zeros.baselibrary.presenter.view

/**
 *  MVP中视图回调 基类  ▏2018/5/15.
 *  wc
 */
interface BaseView {

    fun showLoading()
    fun hideLoading()
    fun onError(text: String)

}