package com.zeros.baselibrary.presenter

import android.content.Context
import com.trello.rxlifecycle.LifecycleProvider
import com.zeros.baselibrary.common.BaseApplication.Companion.context
import com.zeros.baselibrary.presenter.view.BaseView
import com.zeros.baselibrary.utils.NetWorkUtils
import javax.inject.Inject

/**
 *  MVP中P层 基类  ▏2018/5/15.
 */
open class BasePresenter<T : BaseView> {

    // *lateinit 只用于 var，而 lazy 只用于 val
    // *lazy 应用于单例模式(if-null-then-init-else-return)，而且当且仅当变量被第一次调用的时候，委托方法才会执行。
    // *lateinit 则用于只能生命周期流程中进行获取或者初始化的变量，比如 Android 的 onCreate()
    lateinit var mView: T

    //Rx生命周期管理--需要初始化（此处是在BaseMvpActivity中初始化的）
    lateinit var lifecycleProvider: LifecycleProvider<*>

//    lateinit var context: Context   //此处上下文可以通过参数传进来

    //检查网络是否可用--此处也可以用application中的context
    //    BaseApplication.context
    fun checkNetWork(): Boolean {
        if (NetWorkUtils.isNetWorkAvailable(context)) {
            return true
        }
        mView.onError("网络不可用")
        return false
    }


}