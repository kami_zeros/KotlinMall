package com.zeros.baselibrary.ui.fragment

import com.trello.rxlifecycle.components.RxFragment

/**
 *   Fragment基类，业务无关  ▏2018/5/18.
 */
open class BaseFragment : RxFragment()