package com.zeros.baselibrary.ui.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView

/**
 *  RecyclerViewAdapter基类 by MI on 2018/5/20.
 *  wc
 */
abstract class BaseRecyclerViewAdapter<T, VH : RecyclerView.ViewHolder>(var mContext: Context) : RecyclerView.Adapter<VH>() {

    var mItemClickListener: OnItemClickListener<T>? = null      //ItemClick事件
    //数据集合
    var dataList: MutableList<T> = mutableListOf()  //mutableListOf是可变的

    /*-绑定视图-*/
    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.itemView.setOnClickListener {
            if (mItemClickListener != null)
                mItemClickListener!!.onItemClick(dataList[position], position)
        }
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    /*-设置数据:Presenter处理过为null的情况，所以为不会为Null*/
    fun setData(sources: MutableList<T>) {
        dataList = sources
        notifyDataSetChanged()
    }

    /*-ItemCLick的点击事件*/
    interface OnItemClickListener<in T> {
        fun onItemClick(item: T, position: Int)
    }

    fun setOnItemClickListener(listener: OnItemClickListener<T>) {
        this.mItemClickListener = listener
    }


}