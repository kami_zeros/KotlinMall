package com.zeros.baselibrary.ui.activity

import android.os.Bundle
import com.zeros.baselibrary.R
import com.zeros.baselibrary.presenter.BasePresenter
import com.zeros.baselibrary.presenter.view.BaseView
import com.zeros.baselibrary.widgets.ProgressLoading
import org.jetbrains.anko.toast

/**
 *  Activity基类，业务相关  ▏2018/5/15.
 */
open class BaseMvpActivity<T : BasePresenter<*>> : BaseActivity(), BaseView {

    // *lateinit 只用于 var，而 lazy 只用于 val
    // *lazy 应用于单例模式(if-null-then-init-else-return)，而且当且仅当变量被第一次调用的时候，委托方法才会执行。
    // *lateinit 则用于只能生命周期流程中进行获取或者初始化的变量，比如 Android 的 onCreate()
    lateinit var mPresenter: T  //
    private lateinit var mLoadingDialog: ProgressLoading    //懒加载声明

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //初始化加载框
        mLoadingDialog = ProgressLoading.create(this)
    }

    override fun onResume() {
        super.onResume()
        //因为先要mPresenter初始化即在其他Activity中初始化,否则会报异常
        //所以此段代码不能再onCreate方法中，因为这时mPresenter还没有初始化
        mPresenter.let {
            it.lifecycleProvider = this
        }
    }

    /*-显示加载框，默认实现-*/
    override fun showLoading() {
        mLoadingDialog.showLoading()
    }

    /*-隐藏加载框，默认实现-*/
    override fun hideLoading() {
        mLoadingDialog.hideLoading()
    }

    /*-错误信息提示，默认实现-*/
    override fun onError(text: String) {
        toast(text)
    }

}