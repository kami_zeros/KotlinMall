package com.zeros.baselibrary.ui.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.trello.rxlifecycle.components.support.RxAppCompatActivity
import com.zeros.baselibrary.common.AppManager

/**
 * Activity基类，业务无关
 */
open class BaseActivity : RxAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //单例的调用
        AppManager.instance.addActivity(this)
    }


    override fun onDestroy() {
        super.onDestroy()
        AppManager.instance.finishActivity(this)
    }

//    val contentView:View get() {
//
//    }
//
}
