package com.zeros.baselibrary.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.zeros.baselibrary.presenter.BasePresenter
import com.zeros.baselibrary.presenter.view.BaseView
import org.jetbrains.anko.toast

/**
 *  Fragment基类，业务相关  ▏2018/5/18.
 */
open class BaseMvpFragment<T : BasePresenter<*>> : BaseFragment(), BaseView {

    lateinit var mPresenter: T

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        //初始化
        mPresenter.lifecycleProvider = this
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun showLoading() {

    }

    override fun hideLoading() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onError(text: String) {
        toast(text)
    }

}