package com.zeros.baselibrary.common

import android.app.Activity
import android.app.ActivityManager
import android.content.Context
import java.util.*

/**
 *  Activity管理器  ▏2018/5/19.
 *       单例-1.私有构造方法
 *            2. 延迟加载
 *  wc
 */
class AppManager private constructor(){

    private val activityStack: Stack<Activity> = Stack()

    //单例
    companion object {
        val instance: AppManager by lazy { AppManager() }
    }

    /*-1.Activity入栈-*/
    fun addActivity(activity: Activity) {
        activityStack.add(activity)
    }

    /*-2.Activity出栈-*/
    fun finishActivity(activity: Activity) {
        activity.finish()
        activityStack.remove(activity)
    }

    /*-3.获取当前栈顶-*/
    fun currentActivity(): Activity {
        return activityStack.lastElement()
    }

    /*-4.清理栈-*/
    fun finishAllActivity(){
        for (activity in activityStack) {
            activity.finish()
        }
        activityStack.clear()
    }

    /*-5.退出应用程序-*/
    fun exitApp(context: Context) {
        finishAllActivity()
        //as：类的强制转换
        val activityManager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        activityManager.killBackgroundProcesses(context.packageName)
        System.exit(0)
    }

}