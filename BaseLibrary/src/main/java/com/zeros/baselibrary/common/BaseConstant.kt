package com.zeros.baselibrary.common

/**
 *  基础常量  ▏2018/5/16.
 *  wc
 */
class BaseConstant {

    /*-伴随对象:包括属性和方法，和Java中的静态函数和静态属性类似，可以直接通过-类名.**调用-*/
    companion object {
        //七牛服务地址
        const val IMAGE_SERVER_ADDRESS = "http://osea2fxp7.bkt.clouddn.com/"
        //本地服务器地址
        const val SERVER_ADDRESS = "http://192.168.31.116:8080"
        //SP表名
        const val TABLE_PREFS="Kotlin_mall"
        //Token Key
        const val KEY_SP_TOKEN = "token"
    }

}