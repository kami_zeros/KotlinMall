package com.zeros.baselibrary.common

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context

/**
 *  Application 基类  ▏2018/5/17.
 */
class BaseApplication : Application() {

    companion object {
        @SuppressLint("StaticFieldLeak")
        lateinit var context: Context   //懒声明

    }

    override fun onCreate() {
        super.onCreate()

        context = this

    }


}