package com.zeros.baselibrary.widgets

import android.content.Context
import android.widget.ImageView
import com.youth.banner.loader.ImageLoader
import com.zeros.baselibrary.utils.GlideUtils

/**
 *  Banner图片加载器  ▏2018/5/24.
 *      因为引入的是youth.banner包，必须重写loader
 *  wc
 */
class BannerImageLoader : ImageLoader() {

    override fun displayImage(context: Context, path: Any, imageView: ImageView) {
        GlideUtils.loadUrlImage(context, path.toString(), imageView)
    }

}