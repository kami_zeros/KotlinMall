package com.zeros.baselibrary.widgets

import android.app.Activity
import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import com.zeros.baselibrary.R
import com.zeros.baselibrary.ext.onClick
import kotlinx.android.synthetic.main.layout_header_bar.view.*

/**
 *   Header Bar封装  ▏2018/5/19.
 *          @JvmOverloads constructor:通过此方式进行构造方法初始化(
 *              因为@JvmOverloads意思是重载n次方法)
 *   wc
 */
class HeaderBar @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private var isShowBack = true               //是否显示"返回"图标
    private var titleText: String? = null       //Title文字
    private var rightText: String? = null       //右侧文字


    //初始化-自定义的属性
    init {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.HeaderBar)
        isShowBack = typedArray.getBoolean(R.styleable.HeaderBar_isShowBack, true)
        titleText = typedArray.getString(R.styleable.HeaderBar_titleText)
        rightText = typedArray.getString(R.styleable.HeaderBar_rightText)

        initView()
        typedArray.recycle()
    }

    /*-初始化视图-*/
    private fun initView() {
        View.inflate(context, R.layout.layout_header_bar, this)
        mLeftIv.visibility = if (isShowBack) View.VISIBLE else View.GONE

        //a?.b意思是-如果a不为空则返回a.b，否则返回null
        //let函数中：用it表示引用对象即a，并可调用其方法，it不可省略。
        //         返回值是语句块的最后一行，若最后一行语句无返回值，则整个let语句块也无返回值
        //标题titleText不为空，设置值
        titleText?.let {
            mTitleTv.text = it  //it就相当于titleText
        }

        //右侧文字不为空，设置值
        rightText?.let {
            mRightTv.text = it
            mRightTv.visibility= View.VISIBLE
        }

        //返回图标默认实现（关闭Activity）
        mLeftIv.onClick {
            if (context is Activity) {
                (context as Activity).finish()
            }
        }
    }

    /* 获取左侧视图 */
    fun getLeftView(): ImageView {
        return mLeftIv
    }

    /* 获取右侧视图 */
    fun getRightView(): TextView {
        return mRightTv
    }

    /* 获取右侧文字 */
    fun getRightText(): String {
        return mRightTv.text.toString()
    }

}