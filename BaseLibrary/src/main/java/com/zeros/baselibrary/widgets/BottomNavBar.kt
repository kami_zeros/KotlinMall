package com.zeros.baselibrary.widgets

import android.content.Context
import android.util.AttributeSet
import com.ashokvarma.bottomnavigation.BottomNavigationBar
import com.ashokvarma.bottomnavigation.BottomNavigationItem
import com.ashokvarma.bottomnavigation.ShapeBadgeItem
import com.ashokvarma.bottomnavigation.TextBadgeItem
import com.zeros.baselibrary.R

/**
 *  底部导航  ▏2018/5/24.
 *  wc
 */
class BottomNavBar @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : BottomNavigationBar(context, attrs, defStyleAttr) {

    private val mCartBadge: TextBadgeItem   //字体上标,//购物车Tab 标签
    private val mMsgBadge: ShapeBadgeItem   //图形上标,//消息Tab 标签

    init {
        //首页
        val homeItem = BottomNavigationItem(R.mipmap.btn_nav_home_press, resources.getString(R.string.nav_bar_home))
                .setActiveColorResource(R.color.common_blue)     //选择时字体颜色
                .setInactiveIconResource(R.mipmap.btn_nav_home_normal)
                .setInActiveColorResource(R.color.text_normal)

        //分类
        val categoryItem = BottomNavigationItem(R.mipmap.btn_nav_category_press, resources.getString(R.string.nav_bar_category))
                .setInactiveIconResource(R.mipmap.btn_nav_category_normal)
                .setActiveColorResource(R.color.common_blue)
                .setInActiveColorResource(R.color.text_normal)

        //购物车
        val cartItem = BottomNavigationItem(R.mipmap.btn_nav_cart_press, resources.getString(R.string.nav_bar_cart))
                .setInactiveIconResource(R.mipmap.btn_nav_cart_normal)
                .setActiveColorResource(R.color.common_blue)
                .setInActiveColorResource(R.color.text_normal)

        mCartBadge = TextBadgeItem()
//        mCartBadge.setText("10")
        cartItem.setBadgeItem(mCartBadge)

        //消息
        val msgItem = BottomNavigationItem(R.mipmap.btn_nav_msg_press, resources.getString(R.string.nav_bar_msg))
                .setInactiveIconResource(R.mipmap.btn_nav_msg_normal)
                .setActiveColorResource(R.color.common_blue)
                .setInActiveColorResource(R.color.text_normal)

        mMsgBadge = ShapeBadgeItem()
        mMsgBadge.setShape(ShapeBadgeItem.SHAPE_OVAL)   //设置成圆形，默认五角星
        msgItem.setBadgeItem(mMsgBadge)

        //我的
        val userItem = BottomNavigationItem(R.mipmap.btn_nav_user_press, resources.getString(R.string.nav_bar_user))
                .setInactiveIconResource(R.mipmap.btn_nav_user_normal)
                .setActiveColorResource(R.color.common_blue)
                .setInActiveColorResource(R.color.text_normal)

        //设置底部导航模式及样式
        setMode(BottomNavigationBar.MODE_FIXED)     //固定样式
        setBackgroundStyle(BottomNavigationBar.BACKGROUND_STYLE_STATIC)//背景样式
        setBarBackgroundColor(R.color.common_white)//背景颜色

        //初始化所有item-添加Tab
        addItem(homeItem)
                .addItem(categoryItem)
                .addItem(cartItem)
                .addItem(msgItem)
                .addItem(userItem)
                .setFirstSelectedPosition(0)    //默认选择第一个
                .initialise()                    //初始化
    }


    /*-检查购物车Tab是否显示标签,有数量-*/
    fun checkCartBadge(count: Int) {
        if (count == 0) {
            mCartBadge.hide()
        } else {
            mCartBadge.show()
            mCartBadge.setText("$count")
        }
    }

    /*-检查消息Tab是否显示标签,仅有图标-*/
    fun checkMsgBadge(isVisiable: Boolean) {
        if (isVisiable) {
            mMsgBadge.show()
        } else {
            mMsgBadge.hide()
        }
    }


}