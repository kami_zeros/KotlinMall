package com.zeros.baselibrary.widgets

import android.annotation.SuppressLint
import android.content.Context
import android.os.Handler
import android.util.AttributeSet
import android.widget.Button
import com.zeros.baselibrary.R

/**
 *  获取验证码按钮--带倒计时  ▏2018/5/21.
 *  wc
 */
class VerifyButton(mContext: Context, attrs: AttributeSet) : Button(mContext, attrs) {

    private val mHandler: Handler
    private var mCount = 60
    private var mOnVerifyBtnClick: OnVerifyBtnClick? = null

    init {
        this.text = "获取验证码"
        mHandler = Handler()
    }

    /*-倒计时，并处理点击事件-*/
    fun requestSendVerifyNumber() {
        mHandler.postDelayed(countDown, 0)

        if (mOnVerifyBtnClick != null) {
            mOnVerifyBtnClick!!.onClick()
        }
    }

    /*-倒计时-*/
    private val countDown = object : Runnable {
        @SuppressLint("SetTextI18n")
        override fun run() {
            this@VerifyButton.text = mCount.toString() + "s "
            this@VerifyButton.setBackgroundColor(resources.getColor(R.color.common_disable))
            this@VerifyButton.setTextColor(resources.getColor(R.color.common_white))
            this@VerifyButton.isEnabled = false

            if (mCount > 0) {
                mHandler.postDelayed(this, 1000)
            } else {
                resetCounter()  //重置按钮
            }
            mCount--
        }
    }

    /**
     * 按钮恢复到初始状态
     *  @vararg 不定数量参数,一般在最后一个参数，但是如果不在最后一个，
     *          则后面的参数传进来需要使用命名参数语法来传递参数值, 或者, 如果参数类型是函数, 可以在括号之外传递一个
     */
    fun resetCounter(vararg text: String) {
        this.isEnabled = true
        if (text.isNotEmpty() && "" != text[0]) {
            this.text = text[0]
        } else {
            this.text = "重获验证码"
        }
        this.setBackgroundColor(resources.getColor(R.color.transparent))
        this.setTextColor(resources.getColor(R.color.common_blue))
        mCount = 60
    }


    fun removeRunable() {
        mHandler.removeCallbacks(countDown)
    }

    /*-点击事件接口-*/
    interface OnVerifyBtnClick {
        fun onClick()
    }

    fun setOnVerifyBtnClick(onVerifyBtnClick: OnVerifyBtnClick) {
        this.mOnVerifyBtnClick = onVerifyBtnClick
    }


}