package com.zeros.baselibrary.widgets

import android.app.Dialog
import android.content.Context
import android.graphics.drawable.AnimationDrawable
import android.view.Gravity
import android.widget.ImageView
import com.zeros.baselibrary.R
import org.jetbrains.anko.find

/**
 *  加载对话框封装  ▏2018/5/19.
 *  wc
 */
class ProgressLoading private constructor(context: Context, theme: Int) : Dialog(context, theme) {

    //相当于--静态方法
    companion object {
        private lateinit var mDialog: ProgressLoading
        private var animDrawable: AnimationDrawable? = null

        /*-创建加载对话框-*/
        fun create(context: Context): ProgressLoading {
            //样式引入
            mDialog = ProgressLoading(context, R.style.LightProgressDialog)
            //dialog通过此方式进行自定义布局
            mDialog.setContentView(R.layout.progress_dialog)
            mDialog.setCancelable(true)         //是否可以取消
            mDialog.setCanceledOnTouchOutside(false)  //点击外部可以取消
            mDialog.window.attributes.gravity = Gravity.CENTER  //在屏幕中间

            //设置明暗程度
            val lp = mDialog.window.attributes
            lp.dimAmount = 0.2f     //黑暗程度
            //设置属性
            mDialog.window.attributes = lp

            //获取动画视图-------通过此方式获取ImageView
            val loadingView = mDialog.find<ImageView>(R.id.iv_loading)
//            loadingView.background//获取背景色--但是此处是动画所以用以下
            animDrawable = loadingView.background as AnimationDrawable

            return mDialog
        }
    }

    /*-显示加载对话框，动画开始-*/
    fun showLoading() {
        super.show()
        animDrawable?.start()
    }

    /*-隐藏加载对话框，动画停止-*/
    fun hideLoading() {
        super.dismiss()
        animDrawable?.stop()
    }

}