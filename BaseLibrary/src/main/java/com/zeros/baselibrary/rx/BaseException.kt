package com.zeros.baselibrary.rx

/**
 * 定义通用异常   ▏2018/5/16.
 * wc
 */
class BaseException(val status: Int, val msg: String) : Throwable()