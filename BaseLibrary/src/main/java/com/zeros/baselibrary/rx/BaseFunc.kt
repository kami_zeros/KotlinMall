package com.zeros.baselibrary.rx

import com.zeros.baselibrary.common.ResultCode
import com.zeros.baselibrary.data.protocol.BaseResp
import rx.Observable
import rx.functions.Func1

/**
 *  通用数据类型转换封装  ▏2018/5/18.
 *  wc
 */
class BaseFunc<T> : Func1<BaseResp<T>, Observable<T>> {

    override fun call(t: BaseResp<T>): Observable<T> {
        if (t.status != ResultCode.SUCCESS) {
            return Observable.error(BaseException(t.status, t.message))
        }
        return Observable.just(t.data)
    }

}