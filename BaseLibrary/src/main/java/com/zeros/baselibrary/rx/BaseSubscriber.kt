package com.zeros.baselibrary.rx

import com.zeros.baselibrary.presenter.view.BaseView
import rx.Subscriber

/**
 *  Rx订阅者默认实现  ▏2018/5/16.
 *      因为需要进行关闭loading，所以把view通过构造方法传进来
 *  wc
 */
open class BaseSubscriber<T>(val baseView: BaseView) : Subscriber<T>() {

    override fun onNext(t: T) {
    }

    override fun onCompleted() {
        baseView.hideLoading()
    }

    override fun onError(e: Throwable?) {
        baseView.hideLoading()
        if (e is BaseException) {
            baseView.onError(e.msg)
        }
    }
}