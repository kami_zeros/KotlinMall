package com.zeros.baselibrary.utils

import android.annotation.SuppressLint
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 *  日期工具类 默认使用 "yyyy-MM-dd HH:mm:ss" 格式化日期  ▏2018/5/21.
 *  wc
 */
object DateUtils {

    var FORMAT_MONTH_DAY = "MM-dd"      //英文简写（默认）如：12-01
    var FORMAT_SHORT = "yyyy-MM-dd"       //英文简写（默认）如：2010-12-01
    var datePattern = "yyyy-MM-dd HH:mm:ss"//默认,如：2010-12-01 23:15:06
    var FORMAT_LONG_NEW = "yyyy-MM-dd HH:mm"//
    var FORMAT_FULL = "yyyy-MM-dd HH:mm:ss.S"//精确到毫秒的完整时间 如：yyyy-MM-dd HH:mm:ss.S

    var FORMAT_SHORT_CN_MINI = "MM月dd日 HH:mm"//中文简写 如：12月01日 22:23
    var FORMAT_SHORT_CN = "yyyy年MM月dd日"//中文简写 如：2010年12月01日
    var FORMAT_LONG_CN = "yyyy年MM月dd日  HH时mm分ss秒"//中文全称 如：2010年12月01日 23时15分06秒
    var FORMAT_FULL_CN = "yyyy年MM月dd日  HH时mm分ss秒SSS毫秒"//精确到毫秒的完整中文时间
    var FORMAT_SPEFULL_CN = "yyyy年MM月dd日  HH:mm"

    var FORMAT_SHORT_SPE = "yyyyMMdd"
    var FORMAT_SHORT_SPE_ = "HH:mm"
    var TIMEZONE = "Asia/Shanghai"          //时区

    /*-1.根据预设格式返回当前日期,get函数-*/
    val now: String
        get() = format(Date())

    /*-2.获取时区-*/
    val defTimeZone: TimeZone
        get() = TimeZone.getTimeZone(TIMEZONE)

    /*-3.根据用户格式返回当前日期-*/
    fun getNow(format: String): String {
        return format(Date(), format)
    }


    /**
     * 4.使用用户格式格式化日期
     * @JvmOverloads :意思是Kotlin会自动重载成n个方法（n表示参数个数）
     *      format(date)    或
     *      format(date，pattern)
     */
    @SuppressLint("SimpleDateFormat")
    @JvmOverloads
    fun format(date: Date?, pattern: String = datePattern): String {
        var returnValue = ""
        if (date != null) {
            val df = SimpleDateFormat(pattern)
            df.timeZone = defTimeZone
            returnValue = df.format(date)
        }
        return returnValue
    }

    /**
     * 5.使用用户格式提取字符串日期
     * @param strDate  日期字符串
     * @param pattern  日期格式
     */
    @SuppressLint("SimpleDateFormat")
    @JvmOverloads
    fun parse(strDate: String, pattern: String = datePattern): Date? {
        val df = SimpleDateFormat(pattern)
        df.timeZone = defTimeZone
        try {
            return df.parse(strDate)
        } catch (e: ParseException) {
            e.printStackTrace()
            return null
        }
    }


    /*-7.获取当前时间的前一天时间-*/
    fun getBeforeDay(cl: Calendar): Calendar {
        val day = cl.get(Calendar.DATE)
        cl.set(Calendar.DATE, day - 1)
        return cl
    }


    /*-8.获取当前时间的后一天时间-*/
    fun getAfterDay(cl: Calendar): Calendar {
        val day = cl.get(Calendar.DATE)
        cl.set(Calendar.DATE, day + 1)
        return cl
    }


    /*-9.获取一周时间-*/
    fun getWeek(c: Calendar): String {
        var Week = ""
        if (c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            Week += "周日"
        }
        if (c.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
            Week += "周一"
        }
        if (c.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY) {
            Week += "周二"
        }
        if (c.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY) {
            Week += "周三"
        }
        if (c.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY) {
            Week += "周四"
        }
        if (c.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY) {
            Week += "周五"
        }
        if (c.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
            Week += "周六"
        }
        return Week
    }

    /*-6.Long型时间戳转date str-*/
    @SuppressLint("SimpleDateFormat")
    fun convertTimeToString(time: Long, format: String): String {
        val sdf = SimpleDateFormat(format)
        sdf.timeZone = defTimeZone
        return sdf.format(time)
    }

    /*-10.long类型转换为String类型-*/
    @Throws(ParseException::class)
    fun longToString(currentTime: Long, format: String): String {
        val date = longToDate(currentTime, format)
        val strTime = dateToString(date, format)
        return strTime
    }

    /*-11.long转换为Date类型-*/
    @Throws(ParseException::class)
    fun longToDate(currentTime: Long, format: String): Date {
        val dateOld = Date(currentTime)
        val sDateTime = dateToString(dateOld, format)  // 把date类型的时间转换为string
        val date = stringToDate(sDateTime, format)   // 把String类型转换为Date类型
        return date
    }

    /*-12.date类型转换为String类型-*/
    fun dateToString(date: Date, formatType: String): String {
        val sdf = SimpleDateFormat(formatType)
        sdf.timeZone = defTimeZone
        return sdf.format(date)
    }


    /*-13.string类型转换为date类型-*/
    @Throws(ParseException::class)
    fun stringToDate(strTime: String, formatType: String): Date {
        val formatter = SimpleDateFormat(formatType)
        formatter.timeZone = defTimeZone
        var date: Date?
        date = formatter.parse(strTime)
        return date
    }

    /*-14.string类型转换为long类型-*/
    fun stringToLong(strTime: String, formatType: String): Long {
        var date: Date? = null // String类型转成date类型
        try {
            date = stringToDate(strTime, formatType)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return if (date == null) {
            0
        } else {
            dateToLong(date)
        }
    }

    /*-15.date类型转换为long类型*/
    fun dateToLong(date: Date): Long {
        return date.time
    }

    /*-16.当前时间毫秒数-*/
    val curTime: Long
        get() {
            val c = Calendar.getInstance(defTimeZone)
            return c.timeInMillis
        }

}