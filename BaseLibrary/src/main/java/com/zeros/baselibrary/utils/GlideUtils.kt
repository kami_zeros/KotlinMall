package com.zeros.baselibrary.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.zeros.baselibrary.R

/**
 *  Glide工具类  ▏2018/5/21.
 *  wc
 */
object GlideUtils {

    fun loadImage(context: Context, url: String, imageView: ImageView) {
        Glide.with(context)
                .load(url)
                .apply(RequestOptions.centerCropTransform())
                .into(imageView)
    }

    fun loadImageFitCenter(context: Context, url: String, imageView: ImageView) {
        Glide.with(context)
                .load(url)
                .apply(RequestOptions.fitCenterTransform())
                .into(imageView)
    }

    fun loadUrlImage(context: Context, url: String, imageView: ImageView) {
        val options: RequestOptions = RequestOptions()
                .placeholder(R.mipmap.icon_search)
                .error(R.mipmap.icon_search)
                .centerCrop()

        Glide.with(context)
//                .asBitmap()
                .load(url)
                .apply(options)
                .into(object : SimpleTarget<Drawable>() {
//                    override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>) {
//                        imageView.setImageBitmap(resource)
//                    }
                    override fun onResourceReady(resource: Drawable, transition: Transition<in Drawable>) {
                        imageView.setImageDrawable(resource)
                    }
                })
    }

}

