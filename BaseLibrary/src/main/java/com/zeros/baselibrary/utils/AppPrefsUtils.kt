package com.zeros.baselibrary.utils

import android.content.Context
import android.content.SharedPreferences
import com.zeros.baselibrary.common.BaseApplication
import com.zeros.baselibrary.common.BaseConstant

/**
 * SP工具类 by MI on 2018/5/20.
 *  wc
 */
object AppPrefsUtils {

    private var sp: SharedPreferences = BaseApplication.context.getSharedPreferences(BaseConstant.TABLE_PREFS, Context.MODE_PRIVATE)
    private var ed: SharedPreferences.Editor    //声明

    //初始化
    init {
        ed = sp.edit()
    }

    /*-1.存Boolean数据-*/
    fun putBoolean(key: String, value: Boolean) {
        ed.putBoolean(key, value)
        ed.apply()
    }

    /*-1.1.取，默认 false-*/
    fun getBoolean(key: String): Boolean {
        return sp.getBoolean(key, false)
    }

    /*-2.存String数据-*/
    fun putString(key: String, value: String) {
        ed.putString(key, value)
        ed.commit()
    }

    /*-2.1.取，默认 ""*/
    fun getString(key: String): String {
        return sp.getString(key, "")
    }

    /*-3.存Int数据 */
    fun putInt(key: String, value: Int) {
        ed.putInt(key, value)
        ed.apply()
    }

    /*-3.1.取  默认 0*/
    fun getInt(key: String): Int {
        return sp.getInt(key, 0)
    }

    /*-4.存Long数据*/
    fun putLong(key: String, value: Long) {
        ed.putLong(key, value)
        ed.apply()
    }

    /*-4.1.取默认 0*/
    fun getLong(key: String): Long {
        return sp.getLong(key, 0)
    }

    /*-5.Set数据*/
    fun putStringSet(key: String, set: Set<String>) {
        val localSet = getStringSet(key).toMutableSet()
        localSet.addAll(set)
        ed.putStringSet(key, localSet)
        ed.apply()
    }

    /*-5.1默认空set-*/
    fun getStringSet(key: String): Set<String> {
        val set = setOf<String>()
        return sp.getStringSet(key, set)
    }

    /*-6.删除key数据*/
    fun remove(key: String) {
        ed.remove(key)
        ed.apply()
    }

}